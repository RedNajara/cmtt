(function() {
    const myQuestions = [
        {
			id: 1,
            question: "На какую сумму Роспотребнадзор оштрафовал продуктовую сеть «Вкусвилл»?",
            answers: {
                a: "6,3 млн рублей",
                b: "1 млн рублей",
                c: "50 млн рублей",
                d: "Не оштрафовал, а объявил выговор"
            },
            answerTexts: {
                a: "<strong>Верно.</strong> Проверка <a href='https://vc.ru/39208-rospotrebnadzor-oshtrafoval-produktovuyu-set-vkusvill-na-6-3-mln-rubley'>обнаружила</a> просроченные продукты, но представители сети утверждают, что большая часть замечаний не коснулась их качества.",
                b: "<strong>Нет,</strong> немного больше: 6,3 млн рублей. Проверка <a href='https://vc.ru/39208-rospotrebnadzor-oshtrafoval-produktovuyu-set-vkusvill-na-6-3-mln-rubley'>обнаружила</a> просроченные продукты, но представители сети утверждают, что большая часть замечаний не коснулась их качества.",
                c: "<strong>Нет,</strong> гораздо меньше: 6,3 млн рублей. Проверка <a href='https://vc.ru/39208-rospotrebnadzor-oshtrafoval-produktovuyu-set-vkusvill-na-6-3-mln-rubley'>обнаружила</a> просроченные продукты, но представители сети утверждают, что большая часть замечаний не коснулась их качества.",
                d: "<strong>Нет,</strong> всё же оштрафовал на 6,3 млн рублей. Проверка <a href='https://vc.ru/39208-rospotrebnadzor-oshtrafoval-produktovuyu-set-vkusvill-na-6-3-mln-rubley'>обнаружила</a> просроченные продукты, но представители сети утверждают, что большая часть замечаний не коснулась их качества."
            },
            correctAnswer: "a"
        },
        {
			id: 2,
            question: "Почему бутылку воды «Святой источник» в форме футбольного мяча сняли с продажи?",
            answers: {
                a: "Она оказалась огнеопасной",
                b: "Дети играли ею в футбол и разбивали окна",
                c: "Она постоянно скатывалась с полок",
                d: "Её никто не покупал"
            },
			answerTexts: {
                a: "<strong>Да,</strong> бутылка благодаря своей форме призмы фокусировала свет так, что он <a href='https://vc.ru/39121-proizvoditel-vody-svyatoy-istochnik-vypustil-neozhidanno-ogneopasnuyu-butylku-v-forme-myacha'>воспламенял</a> окружающие предметы.",
                b: "<strong>Нет,</strong> дело в её форме призмы — она фокусировала свет так, что он <a href='https://vc.ru/39121-proizvoditel-vody-svyatoy-istochnik-vypustil-neozhidanno-ogneopasnuyu-butylku-v-forme-myacha'>воспламенял</a> окружающие предметы.",
                c: "Бутылка была довольно устойчивая, но благодаря своей форме призмы фокусировала свет так, что он <a href='https://vc.ru/39121-proizvoditel-vody-svyatoy-istochnik-vypustil-neozhidanno-ogneopasnuyu-butylku-v-forme-myacha'>воспламенял</a> окружающие предметы.",
                d: "Её покупали, но бутылка благодаря своей форме призмы фокусировала свет так, что он <a href='https://vc.ru/39121-proizvoditel-vody-svyatoy-istochnik-vypustil-neozhidanno-ogneopasnuyu-butylku-v-forme-myacha'>воспламенял</a> окружающие предметы."
            },
            correctAnswer: "a"
        },
		{
			id: 3,
            question: "Сколько денег потеряли основатели WhatsApp после ухода из  Facebook?",
            answers: {
                a: "Они лишились акций на $1,3 млрд",
                b: "Они не только лишились акций на $1,3 млрд и выплатили штрафы на $100 млн",
                c: "Ничего не лишились, к моменту ухода они уже получили всё вознаграждение",
                d: "Ничего не лишились, но получили иски за нарушение коммерческой тайны"
            },
			answerTexts: {
                a: "Ян Кум и Брайан Эктон действительно могли получить акции на эту сумму, если бы проработали в Facebook до ноября 2018 года. Они ушли досрочно из-за <a href='https://vc.ru/39509-wsj-osnovateli-whatsapp-lishilis-akciy-facebook-na-1-3-mlrd-posle-uhoda-iz-kompanii'>конфликта</a> с руководством.",
                b: "Ян Кум и Брайан Эктон могли получить акции на $1,3 млрд, если бы проработали в Facebook до ноября 2018 года. Они ушли досрочно из-за <a href='https://vc.ru/39509-wsj-osnovateli-whatsapp-lishilis-akciy-facebook-na-1-3-mlrd-posle-uhoda-iz-kompanii'>конфликта</a> с руководством, но штрафы никто не выплачивал.",
                c: "Ян Кум и Брайан Эктон могли получить акции на $1,3 млрд, если бы проработали в Facebook до ноября 2018 года. Они ушли досрочно из-за <a href='https://vc.ru/39509-wsj-osnovateli-whatsapp-lishilis-akciy-facebook-na-1-3-mlrd-posle-uhoda-iz-kompanii'>конфликта</a> с руководством.",
                d: "Ян Кум и Брайан Эктон могли получить акции на $1,3 млрд, если бы проработали в Facebook до ноября 2018 года. Они ушли досрочно из-за <a href='https://vc.ru/39509-wsj-osnovateli-whatsapp-lishilis-akciy-facebook-na-1-3-mlrd-posle-uhoda-iz-kompanii'>конфликта</a> с руководством — но коммерческую тайну пока никто не нарушил."
            },
            correctAnswer: "a"
        },
		{
			id: 4,
            question: "В какой скандал угодила компания «Газелькин» в июне 2018 года?",
            answers: {
                a: "Предложила клиентам вызвать «водителей-славян» за дополнительную плату",
                b: "Её «Газели» постоянно застревали под мостом с надписью «Газель не проедет»",
                c: "Грузчики разбили антикварный шкаф стоимостью 163 млн рублей",
                d: "Выложила персональные данные клиентов — телефоны, адреса, почты — в открытый доступ"
            },
			answerTexts: {
                a: "Компания дала клиентам <a href='https://vc.ru/39346-gruzovaya-kompaniya-gazelkin-predlozhila-klientam-voditeley-slavyan-za-dopolnitelnuyu-platu'>возможность</a> выбрать водителя славянской внешности и российского гражданства. После скандала она переименовала услугу в «идеальный русский язык».",
                b: "Было пару раз, <a href='https://twitter.com/foolsbridge/status/986935782287003649'>но не в июне</a>. На самом деле компания дала клиентам <a href='https://vc.ru/39346-gruzovaya-kompaniya-gazelkin-predlozhila-klientam-voditeley-slavyan-za-dopolnitelnuyu-platu'>возможность</a> выбрать водителя славянской внешности и российского гражданства. После она переименовала услугу в «идеальный русский язык».",
                c: "О таком случае не сообщалось, но зато компания дала клиентам <a href='https://vc.ru/39346-gruzovaya-kompaniya-gazelkin-predlozhila-klientam-voditeley-slavyan-za-dopolnitelnuyu-platu'>возможность</a> выбрать водителя славянской внешности и российского гражданства. После скандала она переименовала услугу в «идеальный русский язык».",
                d: "О таком случае не сообщалось, но зато компания дала клиентам <a href='https://vc.ru/39346-gruzovaya-kompaniya-gazelkin-predlozhila-klientam-voditeley-slavyan-za-dopolnitelnuyu-platu'>возможность</a> выбрать водителя славянской внешности и российского гражданства. После скандала она переименовала услугу в «идеальный русский язык»."
            },
            correctAnswer: "a"
        },
		{
			id: 5,
            question: "Простой вопрос для передышки. До какого порога правительство собирается повысить НДС?",
            answers: {
                a: "20%",
                b: "19%",
                c: "21%",
                d: "Никто не собирается повышать НДС, зато повысят НДФЛ"
            },
			answerTexts: {
                a: "<strong>Да,</strong> НДС <a href='https://vc.ru/40001-medvedev-anonsiroval-povyshenie-nds-do-20'>собираются</a> повысить с 18% до 20%. Кстати, <a href='https://vc.ru/38891-nds-20'>вот чем</a> это грозит",
                b: "<strong>Нет,</strong> всё же именно НДС собираются повысить с 18% до 20%. Кстати, <a href='https://vc.ru/38891-nds-20'>вот чем</a> это грозит",
                c: "<strong>Нет,</strong> НДС <a href='https://vc.ru/40001-medvedev-anonsiroval-povyshenie-nds-do-20'>собираются</a> повысить с 18% до 20%. Кстати, <a href='https://vc.ru/38891-nds-20'>вот чем</a> это грозит",
                d: "<strong>Нет,</strong> всё же именно НДС <a href='https://vc.ru/40001-medvedev-anonsiroval-povyshenie-nds-do-20'>собираются</a>  повысить с 18% до 20%. Кстати, <a href='https://vc.ru/38891-nds-20'>вот чем</a> это грозит"
            },
            correctAnswer: "a"
        },
		{
			id: 6,
            question: "Чем займётся Герман Клименко после ухода с поста советника президента по интернету?",
            answers: {
                a: "Проектами в сфере цифровой медицины",
                b: "Станет разрабатывать собственный мессенджер",
                c: "Будет советником по медицине",
                d: "Отправится в кругосветное путешествие"
            },
			answerTexts: {
                a: "<a href='https://vc.ru/39908-putin-osvobodil-germana-klimenko-ot-dolzhnosti-sovetnika-prezidenta-po-internetu'>Верно</a>",
                b: "Это вполне возможно, но сперва он <a href='https://vc.ru/39908-putin-osvobodil-germana-klimenko-ot-dolzhnosti-sovetnika-prezidenta-po-internetu'>планирует</a> заниматься проектами в сфере цифровой медицины.",
                c: "<strong>Нет</strong>, он <a href='https://vc.ru/39908-putin-osvobodil-germana-klimenko-ot-dolzhnosti-sovetnika-prezidenta-po-internetu'>планирует</a> заниматься собственными проектами в сфере цифровой медицины.",
                d: "Он <a href='https://vc.ru/39908-putin-osvobodil-germana-klimenko-ot-dolzhnosti-sovetnika-prezidenta-po-internetu'>планирует</a> заниматься собственными проектами в сфере цифровой медицины."
            },
            correctAnswer: "a"
        },
		{
			id: 7,
            question: "Какую услугу запустила «Студия Артемия Лебедева» на волне успеха «Экспресс-дизайна»?",
            answers: {
                a: "Экспресс-дизайн не логотипов, а предметов — за 300 тысяч рублей",
                b: "Сервис экспресс-маркетинга: за 500 тысяч рублей Артемий Лебедев лично весь месяц будет вести ваш блог",
                c: "Экспресс-доставку обедов в офис",
                d: "Создание визиток за 50 тысяч рублей"
            },
			answerTexts: {
                a: "<strong>Да,</strong> и заказчик точно так же <a href='https://vc.ru/39869-studiya-artemiya-lebedeva-predlozhila-startapam-ekspress-dizayn-lyubogo-predmeta-za-300-tysyach-rubley'>обязан</a> принять первый предложенный вариант.",
                b: "Речь об экспресс-дизайне предметов, и заказчик точно так же <a href='https://vc.ru/39869-studiya-artemiya-lebedeva-predlozhila-startapam-ekspress-dizayn-lyubogo-predmeta-za-300-tysyach-rubley'>обязан</a> принять первый предложенный вариант.",
                c: "Речь об экспресс-дизайне предметов, и заказчик точно так же <a href='https://vc.ru/39869-studiya-artemiya-lebedeva-predlozhila-startapam-ekspress-dizayn-lyubogo-predmeta-za-300-tysyach-rubley'>обязан</a> принять первый предложенный вариант.",
                d: "Речь об экспресс-дизайне предметов, и заказчик точно так же <a href='https://vc.ru/39869-studiya-artemiya-lebedeva-predlozhila-startapam-ekspress-dizayn-lyubogo-predmeta-za-300-tysyach-rubley'>обязан</a> принять первый предложенный вариант."
            },
            correctAnswer: "a"
        },
		{
			id: 8,
            question: "Что сказал Олег Тиньков в интервью Владимиру Познеру в рамках ПМЭФ-2018?",
            answers: {
                a: "Это стыдно — нанимать людей, которыми нужно управлять",
                b: "Я бы хотел, чтобы на моей гробовой доске было написано слово «Предприниматель»",
                c: "Чтобы быть предпринимателем, не нужно никакого образования",
                d: "Предприниматели должны быть звёздами и элитой общества"
            },
			answerTexts: {
                a: "Вопрос был с подвохом: все эти фразы прозвучали в <a href='https://vc.ru/38748-eto-stydno-nanimat-lyudey-kotorymi-nuzhno-upravlyat'>интервью</a>.",
                b: "Вопрос был с подвохом: все эти фразы прозвучали в <a href='https://vc.ru/38748-eto-stydno-nanimat-lyudey-kotorymi-nuzhno-upravlyat'>интервью</a>.",
                c: "Вопрос был с подвохом: все эти фразы прозвучали в <a href='https://vc.ru/38748-eto-stydno-nanimat-lyudey-kotorymi-nuzhno-upravlyat'>интервью</a>.",
                d: "Вопрос был с подвохом: все эти фразы прозвучали в <a href='https://vc.ru/38748-eto-stydno-nanimat-lyudey-kotorymi-nuzhno-upravlyat'>интервью</a>."
            },
            correctAnswer: "a"
        }
		
    ];
	
	const myQuestionsLength = myQuestions.length;

    function buildBQuiz() {
        const output = [];
		
        myQuestions.forEach((currentQuestion, questionNumber) => {
            const answers = [];
			const answerTexts = [];

        for (letter in currentQuestion.answers) {
            answers.push(
          `<div class="Brt-label-block"><label class="Brt-label Brt-label-${letter}">                
            <input type="radio" name="question${questionNumber}" value="${letter}" class="Brt-radio">
           ${currentQuestion.answers[letter]}		   
        </label></div>	
			<div class="Brt-answer-description">
			${currentQuestion.answerTexts[letter]}		
		</div>`
        );
        }

        output.push(
        `<div class="Brt-slide" data-correct-answer="${currentQuestion.correctAnswer}">
		<div class="Brt-question-count">${currentQuestion.id}/${myQuestionsLength}</div>
        <div class="Brt-question"> ${currentQuestion.question} </div>
        <div class="Brt-answers"> ${answers.join("")} </div>
        </div>`
    );
    });


    quizContainer.innerHTML = output.join("");
}

function showResults() {
    const answerContainers = quizContainer.querySelectorAll(".Brt-answers");
    let numCorrect = 0;
	
	resetButtonWrapper.style.display = "block";
	resetButton.style.display = "flex";
	resultsContainer.style.display = "block";
	quizContainer.style.display = "none";

    myQuestions.forEach((currentQuestion, questionNumber) => {
    const answerContainer = answerContainers[questionNumber];
    const selector = `input[name=question${questionNumber}]:checked`;
    const userAnswer = (answerContainer.querySelector(selector) || {}).value;

    if (userAnswer === currentQuestion.correctAnswer) {		
        numCorrect++;
    } 
});

if (numCorrect === 0) {
	resultsZeroImage.style.display = "block";
	resultsContainerHeader.textContent = "Мне больше интересен футбол";
}

if (0 < numCorrect && numCorrect <= 3) {
	resultsThreeImage.style.display = "block";
	resultsContainerHeader.textContent = "Читаю vc.ru с экрана попутчика в метро";
}

if (3 < numCorrect && numCorrect <= 5) {
	resultsFiveImage.style.display = "block";
	resultsContainerHeader.textContent = "Бизнес это интересно, но где взять столько времени?";
}

if (5 < numCorrect && numCorrect <= 7) {
	resultsSevenImage.style.display = "block";
	resultsContainerHeader.textContent = "Читаю vc.ru каждый день, но работать тоже нужно";
}

if (numCorrect === 8) {
	resultsEightImage.style.display = "block";
	resultsContainerHeader.textContent = "Я работаю в редакции vc.ru";
}

resultsContainerTitle.innerHTML = `${numCorrect} из ${myQuestions.length} правильных ответов`;

}


  let currentSlide = 0;

  function showSlide(n) {
	slides = document.querySelectorAll(".Brt-slide");
	
	if (currentSlide + 1 < slides.length )	{
		slides[currentSlide].classList.remove("Brt-active-slide");  
		slides[n].classList.add("Brt-active-slide");
		currentSlide = n;
	} else {
		showResults();
	}
  }

  function showNextSlide() {
	  
    showSlide(currentSlide + 1);
	
	nextButtonWrapper.style.display = "none";
	
	const labelsBlock = document.querySelectorAll(".Brt-label-block");
	
    for(let i = 0; i < labelsBlock.length; i++){
        labelsBlock[i].style.display = "block";
    }
  }

  const quizContainer = document.getElementById("brt_quiz");
  const resultsContainerTitle = document.getElementById("brt_results_title");
  const resultsContainerHeader = document.getElementById("brt_results_header");
  const resultsContainer = document.getElementById("brt_results");
  const resultsZeroImage = document.getElementById("brt_correct_answers_zero_image");
  const resultsThreeImage = document.getElementById("brt_correct_answers_three_image");
  const resultsFiveImage = document.getElementById("brt_correct_answers_five_image");
  const resultsSevenImage = document.getElementById("brt_correct_answers_seven_image");
  const resultsEightImage = document.getElementById("brt_correct_answers_eight_image"); 
  const resetButton = document.getElementById("brt_reset");
  const startButton = document.getElementById("brt_start");
  const startPage = document.getElementById("b_rules_test_start");
  const nextButton = document.getElementById("brt_next");
  const nextButtonWrapper = document.getElementById("brt_next_wrapper");
  const resetButtonWrapper = document.getElementById("brt_reset_wrapper");
  let slides = document.querySelectorAll(".Brt-slide");

  
  buildBQuiz();  
  

  nextButton.addEventListener("click", showNextSlide);

  startButton.addEventListener("click", startQuiz);

  resetButton.addEventListener("click", resetQuiz);


function addEvent(parent, evt, selector, handler) {
  parent.addEventListener(evt, function(event) {
    if (event.target.matches(selector + ', ' + selector + ' *')) {
      handler.apply(event.target.closest(selector), arguments);
    }
  }, false);
}

function nextElementSibling(element) {
    do { 
        element = element.nextSibling;
    } while (element && element.nodeType !== 1);
    return element;
}

function isCorrectAnswer() {
	let activeSlide = document.querySelector(".Brt-active-slide");
	let activeSlideData = activeSlide.getAttribute('data-correct-answer');
	
	const activeLabelsBlock = activeSlide.querySelectorAll(".Brt-label");
	
    for(let i = 0; i < activeLabelsBlock.length; i++){
        activeLabelsBlock[i].classList.add('Brt-not-correct-answer');
    }
	
	let correctAnswer = activeSlide.querySelectorAll('.Brt-label-'+activeSlideData)[0];
	
	correctAnswer.classList.add('Brt-correct-answer');
}


addEvent(document, 'click', '.Brt-label-block', function(e) {	
	const labelsBlock = document.querySelectorAll(".Brt-label-block");
	
    for(let i = 0; i < labelsBlock.length; i++){
        labelsBlock[i].style.display = "none";
    }
	
	this.style.display = 'block';
	nextElementSibling(this).style.display = 'block';
	
	nextButtonWrapper.style.display = "block";

	isCorrectAnswer();
});
  
  function resetQuiz() {
	resetButton.style.display = "none";
	resultsContainer.style.display = "none";
	startPage.style.display = "block";
	startButton.style.display = "block";
	
	const startImages = document.querySelectorAll(".Brt-image-block");
	
    for(let i = 0; i < startImages.length; i++){
        startImages[i].style.display = "block";
    }	
	
	const imagesResult = document.querySelectorAll(".Brt-results-image");
	
    for(let i = 0; i < imagesResult.length; i++){
        imagesResult[i].style.display = "none";
    }		
	
	buildBQuiz();
  }
  
  function startQuiz() {
	startButton.style.display = "none";
	startPage.style.display = "none";	
	quizContainer.style.display = "block";
	
	const startImages = document.querySelectorAll(".Brt-image-block");
	
    for(let i = 0; i < startImages.length; i++){
        startImages[i].style.display = "none";
    }
	
	currentSlide = 0;

	showSlide(0);
  }

})();